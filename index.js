// console.log("test");

function Trainer(){
	this.trainerName = "Ash";
	this.trainerAge = 17;
	this.trainerPokemon = ["Pikachu","Charizard","Squirtle","Bulbasaur"];
	this.trainerFriends = {
		hoenn : ["May", "Nax"],
		kanto : ["Brocky", "Misty"]
	};

	this.talk = function(){
		console.log(" Pikachu! I choose you!");
	}
};

let trainer = new Trainer();
trainer.talk();

function Pokemon(name, level, health, attack){
	this.pokemonName = name;
	this.pokemonLevel = level;
	this.pokemonHealth = health;
	this.pokemonAttack = attack;

	this.tackle = function(target){
			if(target.pokemonHealth > 0){
				target.pokemonHealth = target.pokemonHealth - this.pokemonAttack;
					if(target.pokemonHealth > 0){
						console.log(this.pokemonName + " dealth " + this.pokemonAttack + "!");
						console.log(target.pokemonName + " has " + target.pokemonHealth + " left.");
					}else{
						console.log(this.pokemonName + " dealth " + this.pokemonAttack + "!");
						console.log(target.pokemonName + "'s' health dropped to zero!");
						target.faint();
					}
			}else{
				console.log(target.pokemonName + " already fainted!");
			}		
	}

	this.faint = function(){
		console.log(this.pokemonName + " fainted!");
	}
}

let pikachu = new Pokemon("Pikachu", 5, 100, 30);
let jigglypuff = new Pokemon("JigglyPuff", 1, 100, 1);

pikachu.tackle(jigglypuff);